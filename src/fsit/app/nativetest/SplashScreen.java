package fsit.app.nativetest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

public class SplashScreen extends Activity{



	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getWindow().addFlags(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);
		
	new Handler().postDelayed(new Runnable(){

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Intent newIntent = new Intent(SplashScreen.this, NativeActivity.class);
			startActivity(newIntent);
			finish();
			
		}},3000);
	}
}
