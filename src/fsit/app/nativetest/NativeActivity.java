package fsit.app.nativetest;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.zxing.client.android.Contents;
import com.google.zxing.client.android.Intents;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.Note;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.CommonDataKinds.Website;
import android.provider.ContactsContract.Contacts.Data;
import android.provider.ContactsContract.Intents.Insert;
import android.text.InputType;
import android.util.Patterns;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NativeActivity extends Activity implements View.OnClickListener{

	Button scan,encodeText,encodeEmai,encodePhone,encodeSms,encodeContact,buttonHelp,buttonCredentials;
	public static final int REQUEST_CODE = 0x0ba7c0de;
	
	Dialog dialogbox,detailsBox;
	EditText value,numberField;
	String enterValue,numberValue;
	private static final String TEXT_TYPE = "TEXT_TYPE";
    private static final String EMAIL_TYPE = "EMAIL_TYPE";
    private static final String PHONE_TYPE = "PHONE_TYPE";
    private static final String SMS_TYPE = "SMS_TYPE";
    private static final String CONTACT_TYPE = "CONTACT_TYPE";
    
    private static final String TEXT_DATA = "ANDROID NATIVE BARCODE TEXT";
    private static final String EMAIL_DATA = "FSIT@FORTUNESOFTIT.COM";
    private static final String PHONE_DATA = "0987654321";
    private static final String SMS_DATA = "0987654321:ANDROID NATIVE BARCODE MESSAGE";
    private static final String CONTACT_DATA = "TEL:0987654321:ANDROID NATIVE BARCODE MESSAGE";
    
    private static final int CONTACT_PICKER_RESULT = 1001;  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_native);
		
		scan =(Button) findViewById(R.id.scan);
		encodeText =(Button) findViewById(R.id.encode_text);
		encodeEmai =(Button) findViewById(R.id.encode_email);
		encodePhone =(Button) findViewById(R.id.encode_phone);
		encodeSms =(Button) findViewById(R.id.encode_sms);
		encodeContact =(Button) findViewById(R.id.encode_contact);
		buttonHelp = (Button) findViewById(R.id.help);
		buttonCredentials = (Button) findViewById(R.id.credentials);
		
		scan.setOnClickListener(this);
		encodeText.setOnClickListener(this);
		encodeEmai.setOnClickListener(this);
		encodePhone.setOnClickListener(this);
		encodeSms.setOnClickListener(this);
		encodeContact.setOnClickListener(this);
		buttonHelp.setOnClickListener(this);
		buttonCredentials.setOnClickListener(this);
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.native_menu, menu);
        return true;
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch(v.getId()){
			case R.id.scan:
				//Intent intentScan = new Intent("com.google.zxing.client.android.SCAN");
				Intent intentScan = new Intent("com.phonegap.plugins.barcodescanner.SCAN");
				intentScan.addCategory(Intent.CATEGORY_DEFAULT);

		        this.startActivityForResult(intentScan, REQUEST_CODE);
				break;
			
			case R.id.encode_text:
				
				
				insertData(0);
				break;
				
			case R.id.encode_email:
				
				insertData(1);
				break;
				
			case R.id.encode_phone:
				
				insertData(2);
				break;
				
			case R.id.encode_sms:
				
				insertData(3);
				break;
				
			case R.id.encode_contact:
				Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,  Contacts.CONTENT_URI);  
			    startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT);  
				break;
				
			case R.id.help:
				Intent help = new Intent(NativeActivity.this,Help.class);
				startActivity(help);
				break;
			case R.id.credentials:
				Intent credentials = new Intent(NativeActivity.this,BRGCredentials.class);
				startActivity(credentials);
				break;
				
		}
	}
	
	
	
	public void insertData(final int choice){
		dialogbox = new Dialog(NativeActivity.this);
		dialogbox.requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(choice ==3){
			dialogbox.setContentView(R.layout.sms_custom);
			numberField = (EditText) dialogbox.findViewById(R.id.number_value); 
			
		}else{
			dialogbox.setContentView(R.layout.custom);
		}
        
		TextView titleText = (TextView) dialogbox.findViewById(R.id.dialog_title);
		TextView detailText = (TextView) dialogbox.findViewById(R.id.title_text);
		value = (EditText) dialogbox.findViewById(R.id.value);
		switch(choice){
		
		case 0:
			
			titleText.setText("INSERT TEXT");
			detailText.setText("Enter text to get the QR Code");
			break;
		case 1:
			
			titleText.setText("INSERT EMAIL");
			detailText.setText("Enter email to get the QR Code");
			value.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
			break;
		case 2:
			
			titleText.setText("INSERT PHONE NUMBER");
			detailText.setText("Enter number to get the QR Code");
			value.setInputType(InputType.TYPE_CLASS_PHONE);
			break;
		case 3:
			
			titleText.setText("INSERT MESSAGE");
			detailText.setText("Enter message to get the QR Code");
			numberField.setInputType(InputType.TYPE_CLASS_PHONE);
			break;
		}
        
        dialogbox.setCancelable(true);
        Button buttonok = (Button) dialogbox.findViewById(R.id.ok);
        buttonok.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
        	String type ="";
    		String data ="";
    		String messsage ="Enter value";
    		boolean satus = true;
        	enterValue = value.getText().toString().trim();
        	if(enterValue!=null && enterValue.length()>0){
        		switch(choice){
            	
            	case 0:
            		type = TEXT_TYPE;
    				data = enterValue;
    				messsage = "Enter some text";
    				encodeData(type,data);
    				break;
            	case 1:
            		type = EMAIL_TYPE;
    				data = enterValue;
    				messsage = "Enter an email address";
    				if(checkEmail(data)){
    					encodeData(type,data);
    				}else{
    					satus =false;
    					showToast(messsage);
    				}
    				
            		break; 
            	case 2:
            		type = PHONE_TYPE;
    				data = enterValue;
    				messsage = "Enter a phone number";
    				if(checkNumber(data)){
    					encodeData(type,data);
    				}else{
    					satus =false;
    					showToast(messsage);
    				}
    				
            		break;
            	case 3:
            		type = SMS_TYPE;
            		numberValue = numberField.getText().toString();
    				data = numberValue+":"+enterValue;
    				messsage = "Enter a phone number";
    				if(checkNumber(numberValue)){
    					encodeData(type,data);
    				}else{
    					satus =false;
    					showToast(messsage);
    				}
            		break;
            	}
        		if(satus)
        		dialogbox.dismiss();
        	}else{
        		Toast.makeText(NativeActivity.this, messsage, Toast.LENGTH_SHORT).show();
        	}
        	
        	
        }
        });
        
        Button buttoncancel = (Button) dialogbox.findViewById(R.id.cancel);
        buttoncancel.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
        	dialogbox.dismiss();
        }
        });
        dialogbox.show();
	}
	
	public void onResume(){
		super.onResume();
	}
	
	public void onPause(){
		super.onPause();
		File[] cacheFiles = NativeActivity.this.getCacheDir().listFiles();

		for (File file : cacheFiles){
			file.delete();
		}
	}
	
	public void onDestroy(){
		File[] cacheFiles = NativeActivity.this.getCacheDir().listFiles();

		for (File file : cacheFiles){
			file.delete();
		}
		super.onDestroy();
	}
	
	public void showToast(String message){
		Toast.makeText(NativeActivity.this, message, Toast.LENGTH_SHORT).show();
	}
	
	private boolean checkEmail(String email){
		/*final String regularExp = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		
		Pattern pattern = Pattern.compile(regularExp);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();*/
        
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
	 }
	
	 private boolean checkNumber(String number){
			Pattern pattern = Pattern.compile("^[0-9]+$");
			Matcher matcher = pattern.matcher(number);
			return matcher.matches();
		}
	 
	public void encodeData(String type,String data ){
		Intent intentEncode = new Intent("com.phonegap.plugins.barcodescanner.ENCODE");
        intentEncode.putExtra("ENCODE_TYPE", type);
        intentEncode.putExtra("ENCODE_DATA", data);
        startActivity(intentEncode);
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                JSONObject obj = new JSONObject();
                try {
                	String text = intent.getStringExtra("SCAN_RESULT");
                	String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                	
                	if(text.startsWith("mailto:")){
                    	
                    	text = text.replace("mailto:", "");
                    	optionSelect(0,text,format);
                    	
                    }else if(text.startsWith("MAILTO:")){
                    	
                    	text = text.replace("MAILTO:", "");
                    	optionSelect(0,text,format);
                    	
                    }else if(text.startsWith("tel:")){
                    	
                    	text = text.replace("tel:", "");
                    	optionSelect(1,text,format);
                    	
                    }else if( text.startsWith("TEL:")){
                    	
                    	text = text.replace("TEL:", "");
                    	optionSelect(1,text,format);
                    	
                    }else if(text.startsWith("sms:")){
                    	
                    	optionSelect(2,text,format);
                    	
                    }else if(text.startsWith("smsto:")){
                    	
                    	optionSelect(2,text,format);
                    	
                    }else if(text.startsWith("SMSTO:")){
                    	
                    	optionSelect(2,text,format);
                    	
                    }else if(text.startsWith("market://")){
                    	
                    	text = text.replace("market://", "https://play.google.com/store/");
                    	optionSelect(3,text,format);
                    	
                    }else if(text.startsWith("MARKET://")){
                    	
                    	text = text.replace("MARKET://", "https://play.google.com/store/");
                    	optionSelect(3,text,format);
                    	
                    }else if(text.startsWith("http://")){
                    	
                    	optionSelect(4,text,format);
                    	
                    }else if(text.startsWith("https://")){
                    	
                    	optionSelect(4,text,format);
                    	
                    }else if(text.startsWith("www.")){
                    	
                    	text = "http://" + text;
                    	optionSelect(4,text,format);
                    	
                    }else if(text.startsWith("HTTP://")){
                    	
                    	optionSelect(4,text,format);
                    	
                    }else if(text.startsWith("HTTPS://")){
                    	
                    	optionSelect(4,text,format);
                    	
                    }else if(text.startsWith("WWW.")){
                    	
                    	text = "http://" + text;
                    	optionSelect(4,text,format);
                    	
                    }else if(text.startsWith("MECARD:")){
                    	
                    	optionSelect(5,text,format);
                    	  
                    }else if(text.startsWith("BEGIN:VCARD:")){
                    	
                    	text = text.replace("BEGIN:VCARD:", "");
                    	text = text.replace("VERSION:", "");
                    	optionSelect(7,text,format);
                    	  
                    }else if(text.startsWith("BEGIN:VCARD")){
                    	
                    	text = text.replace("BEGIN:VCARD", "");
                    	text = text.replace("VERSION:", "");
                    	optionSelect(7,text,format);
                    	
                    }else if(text.startsWith("BIZCARD:")){
                    	text = text.replace("BIZCARD:", "");
                    	optionSelect(8,text,format);
                    	
                    }else if(text.startsWith("maps://")){
                    	
                    	text = text.replace("maps://", "https://maps.google.co.in/maps");
                    	optionSelect(6,text,format);
                    	
                    }else if(text.startsWith("geo:")){
                    	
                    	text = text.replace("geo://", "https://maps.google.co.in/maps?");
                    	optionSelect(6,text,format);
                    	
                    }else if(text.startsWith("MAPS://")){
                    	
                    	text = text.replace("MAPS://", "https://maps.google.co.in/maps");
                    	optionSelect(6,text,format);
                    	
                    }else if(text.startsWith("GEO:")){
                    	
                    	text = text.replace("GEO://", "https://maps.google.co.in/maps?");
                    	optionSelect(6,text,format);
                    	
                    }else if(text.startsWith("MATMSG:")){
                    	text = text.replace("MATMSG:", " ");
                    	optionSelect(9,text,format);
                    }else{
                    	ahowAlert(text, format);
                    }
                	
                	obj.put("text", text);
                    obj.put("format", format);
                    System.out.println(intent.getStringExtra("SCAN_RESULT"));
                    obj.put("cancelled", false);
                }catch(JSONException e) {
                    //Log.d(LOG_TAG, "This should never happen");
                }
                }
		}else if(requestCode == CONTACT_PICKER_RESULT && resultCode == Activity.RESULT_OK){
			Uri contactData = intent.getData();
			readContacts(contactData);
		}
	}
	
	public void readContacts(Uri uri){
        ContentResolver cr = getContentResolver();
        Cursor cur = managedQuery(uri, null, null, null, null);

        String id = "", note="", name ="", phone ="", email="", poBox="";
        String street="", city="", state="", postalCode="", country="";
        String orgName="", title="", website="", url ="", address="";
        
        if (cur.getCount() > 0) {
           while (cur.moveToNext()) {
                id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
               if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                   System.out.println("name : " + name + ", ID : " + id);

                   // get the phone number
                   Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                          ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                                          new String[]{id}, null);
                   while (pCur.moveToNext()) {
                         phone = pCur.getString(
                                pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                         System.out.println("phone" + phone);
                   }
                   pCur.close();

                   // get email and type

                  Cursor emailCur = cr.query(
                           ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                           null,
                           ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                           new String[]{id}, null);
                   while (emailCur.moveToNext()) {
                       // This would allow you get several email addresses
                           // if the email addresses were stored in an array
                       email = emailCur.getString(
                                     emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                       String emailType = emailCur.getString(
                                     emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));

                     System.out.println("Email " + email + " Email Type : " + emailType);
                   }
                   emailCur.close();

                   // Get note.......
                   String noteWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                   String[] noteWhereParams = new String[]{id,
                   ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE};
                           Cursor noteCur = cr.query(ContactsContract.Data.CONTENT_URI, null, noteWhere, noteWhereParams, null);
                   if (noteCur.moveToFirst()) {
                       note = noteCur.getString(noteCur.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
                     System.out.println("Note " + note);
                   }
                   noteCur.close();

                   //Get Postal Address....

                   String addrWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                   String[] addrWhereParams = new String[]{id,
                       ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE};
//              Cursor addrCur = cr.query(ContactsContract.Data.CONTENT_URI,
//                      null, null, null, null);
              Cursor addrCur = cr.query(ContactsContract.Data.CONTENT_URI,
                      null, addrWhere, addrWhereParams, null);

                   while(addrCur.moveToNext()) {
                       poBox = addrCur.getString(
                                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POBOX));
                       street = addrCur.getString(
                                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                       city = addrCur.getString(
                                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                       state = addrCur.getString(
                                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION));
                       postalCode = addrCur.getString(
                                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
                       country = addrCur.getString(
                                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
                       String type = addrCur.getString(
                                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE));

                       address = street+" "+city+" "+state+" "+postalCode;
                       // Do something with these...."123 Fake St. San Francisco, CA 94102"
                       
                       System.out.println("poBox " + address);
                       
                       

                   }
                   addrCur.close();

                   // Get Instant Messenger.........
                   String imWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                   String[] imWhereParams = new String[]{id,
                       ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE};
                   Cursor imCur = cr.query(ContactsContract.Data.CONTENT_URI,
                           null, imWhere, imWhereParams, null);
                   if (imCur.moveToFirst()) {
                       String imName = imCur.getString(
                                imCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA));
                       String imType;
                       imType = imCur.getString(
                                imCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.TYPE));
                   }
                   imCur.close();

                   // Get Organizations.........

                   String orgWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                   String[] orgWhereParams = new String[]{id,
                       ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE};
                   Cursor orgCur = cr.query(ContactsContract.Data.CONTENT_URI,
                               null, orgWhere, orgWhereParams, null);
                   if (orgCur.moveToFirst()) {
                       orgName = orgCur.getString(orgCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DATA));
                       title = orgCur.getString(orgCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
                   }
                   orgCur.close();
                   
                   //Get website.....
                   
                   String websiteWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                   String[] websiteWhereParams = new String[]{id,
                       ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE};
                   Cursor webCur = cr.query(ContactsContract.Data.CONTENT_URI,
                               null, websiteWhere, websiteWhereParams, null);
                   if (webCur.moveToFirst()) {
                       website = webCur.getString(webCur.getColumnIndex(ContactsContract.CommonDataKinds.Website.DATA));
                       url = webCur.getString(webCur.getColumnIndex(ContactsContract.CommonDataKinds.Website.URL));
                       
                   }
                   webCur.close();
                   
                   
               }
           }
      }
        Bundle bundle = new Bundle(); 
        bundle.putString(ContactsContract.Intents.Insert.NAME, name); 
        bundle.putString(Contents.PHONE_KEYS[0], phone); 
        bundle.putString(Contents.EMAIL_KEYS[0], email); 
        bundle.putString(ContactsContract.Intents.Insert.POSTAL, address); 
        showDetails(bundle);
   }
	
	private void showDetails(final Bundle bundle){
		
		TextView contactName,contactNumber,contactEmail,ContactAddress,contactWebsite;
		
		detailsBox = new Dialog(NativeActivity.this);
		detailsBox.requestWindowFeature(Window.FEATURE_NO_TITLE);
		detailsBox.setContentView(R.layout.details);
		detailsBox.setCancelable(true);
		contactName =(TextView) detailsBox.findViewById(R.id.name_value);
		contactNumber =(TextView) detailsBox.findViewById(R.id.phone_value);
		contactEmail =(TextView) detailsBox.findViewById(R.id.mail_value);
		ContactAddress =(TextView) detailsBox.findViewById(R.id.address_value);
		contactWebsite =(TextView) detailsBox.findViewById(R.id.website_value);
		
		contactName.setText(bundle.getString(ContactsContract.Intents.Insert.NAME));
		contactNumber.setText(bundle.getString(Contents.PHONE_KEYS[0]));
		contactEmail.setText(bundle.getString(Contents.EMAIL_KEYS[0]));
		ContactAddress.setText(bundle.getString(ContactsContract.Intents.Insert.POSTAL));
		
        Button buttonok = (Button) detailsBox.findViewById(R.id.contact_ok);
        buttonok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				detailsBox.dismiss();
				Intent intentEncode = new Intent("com.phonegap.plugins.barcodescanner.ENCODE");
		        intentEncode.putExtra("ENCODE_TYPE", CONTACT_TYPE);
		        intentEncode.putExtra("ENCODE_DATA", bundle);
		        startActivity(intentEncode);
			}});
        
        Button buttoncancel = (Button) detailsBox.findViewById(R.id.contact_cancel);
        buttoncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				detailsBox.dismiss();
			}});
        detailsBox.show();
		
	}

	private void ahowAlert(String text, String format) {
		// TODO Auto-generated method stub
		
		final Dialog optionBox = new Dialog(NativeActivity.this);
		optionBox.requestWindowFeature(Window.FEATURE_NO_TITLE);
		optionBox.setContentView(R.layout.option);
		optionBox.setCancelable(true);
		TextView messageView = (TextView) optionBox.findViewById(R.id.action_text);
		TextView messageTitle = (TextView) optionBox.findViewById(R.id.dialog_title);
		Button buttoncancel = (Button) optionBox.findViewById(R.id.action_cancel);
		messageTitle.setText("RESPONSE");
		buttoncancel.setVisibility(View.GONE);
		
		String message =text;
		messageView.setText(message);
		Button buttonok = (Button) optionBox.findViewById(R.id.action_ok);
        buttonok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				optionBox.dismiss();
				
			}});
		
		optionBox.show();
	}
	
	private void optionSelect(final int option, final String data,String format){
		final Dialog optionBox = new Dialog(NativeActivity.this);
		optionBox.requestWindowFeature(Window.FEATURE_NO_TITLE);
		optionBox.setContentView(R.layout.option);
		optionBox.setCancelable(true);
		TextView messageView = (TextView) optionBox.findViewById(R.id.action_text);
		
		String message = data;
		switch(option){
			
		case 0: 
			message = "Preview and send the mail";
			break;
		case 1: 
			message = "Preview and call the number";
			break;
		case 2: 
			message = "Preview and send message to the number";
			break;
		case 3: 
			message = "Preview the application";
			break;
		case 4: 
			message = "View the website";
			break;
		case 5:
			message = "Preview and save the contact";
			break;
		case 6:
			message = "View in google map";
			break;
		case 7:
			message = "Preview and save the contact";
			break;
		case 8:
			message = "Preview and save the contact";
			break;
		case 9:
			message = "Preview and send the mail";
			break;
		}
		
		messageView.setText(message);
		
		Button buttonok = (Button) optionBox.findViewById(R.id.action_ok);
        buttonok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				optionBox.dismiss();
				action(option,data);
			}});
        
        Button buttoncancel = (Button) optionBox.findViewById(R.id.action_cancel);
        buttoncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				optionBox.dismiss();
				
			}});
        
        optionBox.show();
	}
	
	public void action(int choice, String data){
		switch(choice){
		
		case 0:
			try {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("message/rfc822");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{data});
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "BRG APPLICATION");
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "From BRG android application");

            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        }
        catch (Exception ex) {}
			break;
			
		case 1:
			Uri number = Uri.parse("tel:" + data);
            Intent dial = new Intent(Intent.ACTION_DIAL, number);
            startActivity(dial);
            
			break;
			
		case 2:
			String[] group = data.split(":");
        	String phone_number="";
        	String message="";
        	if(group.length>2){
        		phone_number = group[1];
        		message = group[2];
        	}else{
        		if(group[1].matches("^[0-9- ]+$") ){
        			phone_number = group[1];
        		}else{
        			message = group[1];
        		}
        	}
        	if(data.startsWith("sms:")){
        		data = data.replace("sms:", "");
        	}else if(data.startsWith("smsto:")){
        		data = data.replace("smsto:", "");
        	}else if(data.startsWith("SMSTO:")){
        		data = data.replace("SMSTO:", "");
        	}
        	String uri= "smsto: "+phone_number;
            Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse(uri));
            smsIntent.putExtra("sms_body", message);
            smsIntent.putExtra("compose_mode", true);
            startActivity(smsIntent);
        
			break;
			
		case 3:
			Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
            startActivity(appIntent);
			break;
			
		case 4: 
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
            startActivity(browserIntent);
			break;
			
		case 5:
			
			
			ArrayList<ContentValues> text = new ArrayList<ContentValues>();
        	String name="",email="",address="",websit="",phone="", org="";
        	
        	int currentapiVersion = android.os.Build.VERSION.SDK_INT;
             Pattern emailPattern = Pattern.compile("EMAIL:+(.*?);");
             Matcher matcher = emailPattern.matcher(data);
             if (matcher.find()) {
            	   email = matcher.group(1);
             }
        	
             Pattern urlPattern = Pattern.compile("URL:+(.*?);");
              matcher = urlPattern.matcher(data);
              if (matcher.find()) {
              	   websit = matcher.group(1);
              }
             
              
              Pattern namePattern = Pattern.compile("N:+(.*?);");
              matcher = namePattern.matcher(data);
              if (matcher.find()) {
              	   name = matcher.group(1);
              }
              
              Pattern addPattern = Pattern.compile("ADR:+(.*?);");
              matcher = addPattern.matcher(data);
              if (matcher.find()) {
              	   address = matcher.group(1);
              }
              
              Pattern numPattern = Pattern.compile("TEL:+(.*?);");
              matcher = numPattern.matcher(data);
              if (matcher.find()) {
              	   phone = matcher.group(1);
              }
              Pattern orgPattern = Pattern.compile("ORG:+(.*?);");
              matcher = orgPattern.matcher(data);
              if (matcher.find()) {
              	   org = matcher.group(1);
              }
              
              if(currentapiVersion>=11){
              
            	//Email
            	  ContentValues row1 = new ContentValues();
            	  row1.put(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE);
            	  row1.put(Email.ADDRESS, email);
            	  text.add(row1);
            	  
            	 
            	  
            	//Website
            	  ContentValues row2 = new ContentValues();
            	  row2.put(Data.MIMETYPE, Website.CONTENT_ITEM_TYPE);
            	  row2.put(Website.URL, websit);
            	  text.add(row2);
            	  
            	  
            	  //name
            	  ContentValues row3 = new ContentValues();
            	  row3.put(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE);
            	  row3.put(StructuredName.DISPLAY_NAME, name);
            	  text.add(row3);
            	  
            	  
            	  
            	  //Address
            	  String [] list = address.split(" ");
            	  
            	  ContentValues row4 = new ContentValues();
            	  row4.put(Data.MIMETYPE, StructuredPostal.CONTENT_ITEM_TYPE);
            	  row4.put(StructuredPostal.CITY, list[0]);
            	  row4.put(StructuredPostal.COUNTRY, list[3]);
            	  row4.put(StructuredPostal.STREET, list[1]);
            	  row4.put(StructuredPostal.POSTCODE, list[2]);
            	  text.add(row4);
            	  
            	  
            	  
            	  //Phone 
            	  
            	  ContentValues row5 = new ContentValues();
            	  row5.put(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
            	  row5.put(Phone.NUMBER, phone);
            	  text.add(row5);
            	  
            	  //company
            	  
            	  ContentValues row6 = new ContentValues();
            	  row6.put(Data.MIMETYPE, Organization.CONTENT_ITEM_TYPE);
            	  row6.put(Organization.COMPANY, org);
            	  text.add(row6);
            	  
            	  
            		  Intent i = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
                	  i.putParcelableArrayListExtra(Insert.DATA, text);
                	  startActivity(i);
        	  }else{
        		  Intent i = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
        		  i.setType(ContactsContract.Contacts.CONTENT_TYPE);
        		  i.putExtra(ContactsContract.Intents.Insert.NAME, name);
        		  i.putExtra(ContactsContract.Intents.Insert.PHONE, phone);
        		  i.putExtra(ContactsContract.Intents.Insert.POSTAL, address);
        		  i.putExtra(ContactsContract.Intents.Insert.EMAIL, email);
        		  i.putExtra(ContactsContract.Intents.Insert.NOTES, websit);
        		  i.putExtra(ContactsContract.Intents.Insert.COMPANY, org);
        		  startActivity(i);
        	  }
			break;
			
		case 6:
			Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
            startActivity(mapIntent);
			break;
			
		case 7:
			
			
			
			
			ArrayList<ContentValues> newtext = new ArrayList<ContentValues>();
        	String newname="",newemail="",newaddress="",newwebsit="",newphone="", neworg="", newtitle="";
        	
        	int newcurrentapiVersion = android.os.Build.VERSION.SDK_INT;
        	
        	
        	Pattern newnamePattern = Pattern.compile("N:+(.*?)\n");
			Matcher newmatcher = newnamePattern.matcher(data);
			if (newmatcher.find()) {
         	 String  tempname = newmatcher.group(1);
         	  String[] namePart= tempname.split(";");
         	  if(namePart.length>1){
         		 newname = namePart[1]+" "+namePart[0];
         	  }else{
         		 newname = newmatcher.group(1);
         	  }
			}else{
				newnamePattern = Pattern.compile("N:+(.*?)\r");
				newmatcher = newnamePattern.matcher(data);
				if (newmatcher.find()) {
					String  tempname = newmatcher.group(1);
		         	  String[] namePart= tempname.split(";");
		         	  if(namePart.length>1){
		         		 newname = namePart[1]+" "+namePart[0];
		         	  }else{
		         		 newname = newmatcher.group(1);
		         	  }
				}else{
					newnamePattern = Pattern.compile("N:+(.*?);");
					newmatcher = newnamePattern.matcher(data);
					if (newmatcher.find()) {
						newname = newmatcher.group(1);
					}
				}
			}
			
			
             Pattern newemailPattern = Pattern.compile("EMAIL;TYPE=PREF,INTERNET:+(.*?)\n");
             newmatcher = newemailPattern.matcher(data);
             if (newmatcher.find()) {
            	   newemail = newmatcher.group(1);
            	   
             }else{
            	 newemailPattern = Pattern.compile("EMAIL;TYPE=PREF,INTERNET:+(.*?)\r");
                 newmatcher = newemailPattern.matcher(data);
                 if (newmatcher.find()) {
                	   newemail = newmatcher.group(1);
                	   
                 }else{
                	 newemailPattern = Pattern.compile("EMAIL:+(.*?)\n");
                     newmatcher = newemailPattern.matcher(data);
                     if (newmatcher.find()) {
                    	   newemail = newmatcher.group(1);
                    	   
                     }else{
                    	 newemailPattern = Pattern.compile("EMAIL:+(.*?)\r");
                         newmatcher = newemailPattern.matcher(data);
                         if (newmatcher.find()) {
                        	   newemail = newmatcher.group(1);
                        	   
                         }else{
                        	 newemailPattern = Pattern.compile("EMAIL;INTERNET:+(.*?)\n");
                             newmatcher = newemailPattern.matcher(data);
                             if (newmatcher.find()) {
                            	   newemail = newmatcher.group(1);
                            	   
                             }else{
                            	 newemailPattern = Pattern.compile("EMAIL;INTERNET:+(.*?)\r");
                                 newmatcher = newemailPattern.matcher(data);
                                 if (newmatcher.find()) {
                                	   newemail = newmatcher.group(1);
                                	   
                                 }else{
                                	 newemailPattern = Pattern.compile("EMAIL:+(.*?);");
                                     newmatcher = newemailPattern.matcher(data);
                                     if (newmatcher.find()) {
                                    	   newemail = newmatcher.group(1);
                                    	   
                                     }else{
                                    	 newemailPattern = Pattern.compile("EMAIL;type=INTERNET;type=WORK:+(.*?);");
                                         newmatcher = newemailPattern.matcher(data);
                                         if (newmatcher.find()) {
                                        	   newemail = newmatcher.group(1);
                                        	   
                                         }else{
                                        	 newemailPattern = Pattern.compile("EMAIL;type=INTERNET;type=WORK:+(.*?)\n");
                                             newmatcher = newemailPattern.matcher(data);
                                             if (newmatcher.find()) {
                                            	   newemail = newmatcher.group(1);
                                            	   
                                             }else{
                                            	 newemailPattern = Pattern.compile("EMAIL;type=INTERNET;type=WORK:+(.*?)\r");
                                                 newmatcher = newemailPattern.matcher(data);
                                                 if (newmatcher.find()) {
                                                	   newemail = newmatcher.group(1);
                                                	   
                                                 }else{
                                                	 newemailPattern = Pattern.compile("EMAIL;TYPE=INTERNET;TYPE=WORK:+(.*?);");
                                                     newmatcher = newemailPattern.matcher(data);
                                                     if (newmatcher.find()) {
                                                    	   newemail = newmatcher.group(1);
                                                    	   
                                                     }else{
                                                    	 newemailPattern = Pattern.compile("EMAIL;TYPE=INTERNET;TYPE=WORK:+(.*?)\n");
                                                         newmatcher = newemailPattern.matcher(data);
                                                         if (newmatcher.find()) {
                                                        	   newemail = newmatcher.group(1);
                                                        	   
                                                         }else{
                                                        	 newemailPattern = Pattern.compile("EMAIL;TYPE=INTERNET;type=WORK:+(.*?)\r");
                                                             newmatcher = newemailPattern.matcher(data);
                                                             if (newmatcher.find()) {
                                                            	   newemail = newmatcher.group(1);
                                                            	   
                                                             }else{
                                                            	 newemailPattern = Pattern.compile("EMAIL;PREF;INTERNET:+(.*?)\r");
                                                                 newmatcher = newemailPattern.matcher(data);
                                                                 if (newmatcher.find()) {
                                                                	   newemail = newmatcher.group(1);
                                                                	   
                                                                 }else{
                                                                	 newemailPattern = Pattern.compile("EMAIL;TYPE=internet:+(.*?)\r");
                                                                     newmatcher = newemailPattern.matcher(data);
                                                                     if (newmatcher.find()) {
                                                                    	   newemail = newmatcher.group(1);
                                                                    	   
                                                                     }else{
                                                                    	 newemailPattern = Pattern.compile("EMAIL;TYPE=internet:+(.*?)\n");
                                                                         newmatcher = newemailPattern.matcher(data);
                                                                         if (newmatcher.find()) {
                                                                        	   newemail = newmatcher.group(1);
                                                                        	   
                                                                         }else{
                                                                        	 newemailPattern = Pattern.compile("EMAIL;+(.*?)\n");
                                                                             newmatcher = newemailPattern.matcher(data);
                                                                             if (newmatcher.find()) {
                                                                            	 String[] parts = newmatcher.group(1).split(":");
                                                                            	 if(parts.length>1){
                                                                            		 newemail = parts[parts.length-1];
                                                                            	 }else{
                                                                            		 newemail = newmatcher.group(1); 
                                                                            	 }
                                                                            	   
                                                                            	   
                                                                             }else{
                                                                            	 newemailPattern = Pattern.compile("EMAIL;+(.*?)\r");
                                                                                 newmatcher = newemailPattern.matcher(data);
                                                                                 if (newmatcher.find()) {
                                                                                	 String[] parts = newmatcher.group(1).split(":");
                                                                                	 if(parts.length>1){
                                                                                		 newemail = parts[parts.length-1];
                                                                                	 }else{
                                                                                		 newemail = newmatcher.group(1); 
                                                                                	 }
                                                                                	   
                                                                                	   
                                                                                 }
                                                                             }
                                                                         }
                                                                     }
                                                                 }
                                                             }
                                                         }
                                                     }
                                                 }
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
        	
             Pattern newurlPattern = Pattern.compile("URL:+(.*?)\n");
             newmatcher = newurlPattern.matcher(data);
              if (newmatcher.find()) {
              	   newwebsit = newmatcher.group(1);
              	
              }else{
            	  newurlPattern = Pattern.compile("URL:+(.*?)\r");
                  newmatcher = newurlPattern.matcher(data);
                   if (newmatcher.find()) {
                   	   newwebsit = newmatcher.group(1);
                   	
                   }else{
                	   newurlPattern = Pattern.compile("URL:+(.*?);");
                       newmatcher = newurlPattern.matcher(data);
                        if (newmatcher.find()) {
                        	   newwebsit = newmatcher.group(1);
                        	
                        }else{
                        	newurlPattern = Pattern.compile("URL;type=WORK:+(.*?);");
                            newmatcher = newurlPattern.matcher(data);
                             if (newmatcher.find()) {
                             	   newwebsit = newmatcher.group(1);
                             	
                             }else{
                            	 newurlPattern = Pattern.compile("URL;type=WORK:+(.*?)\n");
                                 newmatcher = newurlPattern.matcher(data);
                                  if (newmatcher.find()) {
                                  	   newwebsit = newmatcher.group(1);
                                  	
                                  }else{
                                	  newurlPattern = Pattern.compile("URL;type=WORK:+(.*?)\r");
                                      newmatcher = newurlPattern.matcher(data);
                                       if (newmatcher.find()) {
                                       	   newwebsit = newmatcher.group(1);
                                       	
                                       }else{
                                    	   newurlPattern = Pattern.compile("URL;TYPE=WORK:+(.*?);");
                                           newmatcher = newurlPattern.matcher(data);
                                            if (newmatcher.find()) {
                                            	   newwebsit = newmatcher.group(1);
                                            	
                                            }else{
                                            	newurlPattern = Pattern.compile("URL;TYPE=WORK:+(.*?)\n");
                                                newmatcher = newurlPattern.matcher(data);
                                                 if (newmatcher.find()) {
                                                 	   newwebsit = newmatcher.group(1);
                                                 	
                                                 }else{
                                                	 newurlPattern = Pattern.compile("URL;TYPE=WORK:+(.*?)\r");
                                                     newmatcher = newurlPattern.matcher(data);
                                                      if (newmatcher.find()) {
                                                      	   newwebsit = newmatcher.group(1);
                                                      	
                                                      }else{
                                                    	  newurlPattern = Pattern.compile("URL;+(.*?)\n");
                                                          newmatcher = newurlPattern.matcher(data);
                                                           if (newmatcher.find()) {
                                                        	   String[] urlParts = newmatcher.group(1).split(":");
                                                        	   if(urlParts.length>1){
                                                        		   newwebsit = urlParts[urlParts.length-1];
                                                        	   }else{
                                                        		   newwebsit = newmatcher.group(1);
                                                        	   }
                                                           	                                                              	
                                                           }else{
                                                        	   newurlPattern = Pattern.compile("URL;+(.*?)\r");
                                                               newmatcher = newurlPattern.matcher(data);
                                                                if (newmatcher.find()) {
                                                             	   String[] urlParts = newmatcher.group(1).split(":");
                                                             	   if(urlParts.length>1){
                                                             		   newwebsit = urlParts[urlParts.length-1];
                                                             	   }else{
                                                             		   newwebsit = newmatcher.group(1);
                                                             	   }
                                                                	                                                              	
                                                                }
                                                           }
                                                      }
                                                 }
                                            }
                                       }
                                  }
                             }
                        }
                   }
              }
             
              Pattern newaddPattern = Pattern.compile("ADR;TYPE=WORK:;;+(.*?)\n");
              newmatcher = newaddPattern.matcher(data);
              if (newmatcher.find()) {
              	   newaddress = newmatcher.group(1);
              	 
              }else{
            	  newaddPattern = Pattern.compile("ADR;TYPE=WORK:;;+(.*?)\r");
                  newmatcher = newaddPattern.matcher(data);
                  if (newmatcher.find()) {
                  	   newaddress = newmatcher.group(1);
                  	 
                  }else{
                	  newaddPattern = Pattern.compile("ADR;type=WORK:;;+(.*?)\n");
                	  newmatcher = newaddPattern.matcher(data);
                	  if (newmatcher.find()) {
                     	   newaddress = newmatcher.group(1);
                     	  
                	  }else{
                		  newaddPattern = Pattern.compile("ADR;type=WORK:;;+(.*?)\r");
                    	  newmatcher = newaddPattern.matcher(data);
                    	  if (newmatcher.find()) {
                         	   newaddress = newmatcher.group(1);
                         	  
                    	  }else{
                    		  newaddPattern = Pattern.compile("ADR;TYPE=HOME:;;+(.*?)\n");
                        	  newmatcher = newaddPattern.matcher(data);
                        	  if (newmatcher.find()) {
                             	   newaddress = newmatcher.group(1);
                             	  
        		               }else{
        		            	   newaddPattern = Pattern.compile("ADR;TYPE=HOME:;;+(.*?)\r");
        		                	  newmatcher = newaddPattern.matcher(data);
        		                	  if (newmatcher.find()) {
        		                     	   newaddress = newmatcher.group(1);
        		                     	  
        				               }else{
        				            	   newaddPattern = Pattern.compile("ADR;type=HOME:;;+(.*?)\n");
        					            	  newmatcher = newaddPattern.matcher(data);
        					            	  if (newmatcher.find()) {
        					                 	   newaddress = newmatcher.group(1);
        					                 	  
        					            	  }else{
        					            		  newaddPattern = Pattern.compile("ADR;type=HOME:;;+(.*?)\r");
        						            	  newmatcher = newaddPattern.matcher(data);
        						            	  if (newmatcher.find()) {
        						                 	   newaddress = newmatcher.group(1);
        						                 	  
        						            	  }else{
        						            		  newaddPattern = Pattern.compile("ADR:;;+(.*?)\n");
        							               	  newmatcher = newaddPattern.matcher(data);
        							               	  if (newmatcher.find()) {
        							                    	   newaddress = newmatcher.group(1);
        							                    	  
        							                   }else{
        							                	   newaddPattern = Pattern.compile("ADR:;;+(.*?)\r");
        									               	  newmatcher = newaddPattern.matcher(data);
        									               	  if (newmatcher.find()) {
        									                    	   newaddress = newmatcher.group(1);
        									                    	  
        									                   }else{
        									                	   newaddPattern = Pattern.compile("ADR;WORK:;;+(.*?)\n");
        									                     	  newmatcher = newaddPattern.matcher(data);
        									                     	  if (newmatcher.find()) {
        									                          	   newaddress = newmatcher.group(1);
        									                          	  
        									                     	  }else{
        									                     		 newaddPattern = Pattern.compile("ADR;WORK:;;+(.*?)\r");
        										                     	  newmatcher = newaddPattern.matcher(data);
        										                     	  if (newmatcher.find()) {
        										                          	   newaddress = newmatcher.group(1);
        										                          	  
        										                     	  }else{
        										                     		 newaddPattern = Pattern.compile("ADR;HOME:;;+(.*?)\n");
        										                        	  newmatcher = newaddPattern.matcher(data);
        										                        	  if (newmatcher.find()) {
        										                             	   newaddress = newmatcher.group(1);
        										                             	
        										                     		  
        										                     		 	}else{
        										                     		 		newaddPattern = Pattern.compile("ADR;HOME:;;+(.*?)\r");
        												                        	  newmatcher = newaddPattern.matcher(data);
        												                        	  if (newmatcher.find()) {
        												                             	   newaddress = newmatcher.group(1);
        												                             	
        												                     		  
        												                     		 	}else{
        												                     		 		newaddPattern = Pattern.compile("ADR:+(.*?);");
        													  	                        	  newmatcher = newaddPattern.matcher(data);
        													  	                        	  if (newmatcher.find()) {
        													  	                             	   newaddress = newmatcher.group(1);
        													  	                             	
        													  	                     		  
        													  	                     		 	}
        												                     		 	}
        										                     		 	}
        										                     	  }
        									                     	  }
        									                   }
        							                   }
        						            	  }
        					            	  }
        				               }
        		               }
                    	  }
                	  }
                  }
              }
              
              
              Pattern newnumPattern = Pattern.compile("TEL;TYPE=WORK,VOICE:+(.*?)\n");
              newmatcher = newnumPattern.matcher(data);
              if (newmatcher.find()) {
              	   newphone = newmatcher.group(1);
              	 
              }else{
            	  newnumPattern = Pattern.compile("TEL;TYPE=WORK:+(.*?)\n");
                  newmatcher = newnumPattern.matcher(data);
                  if (newmatcher.find()) {
                  	   newphone = newmatcher.group(1);
                  	 
                  }else{
                	  newnumPattern = Pattern.compile("TEL;type=WORK:+(.*?)\n");
                      newmatcher = newnumPattern.matcher(data);
                      if (newmatcher.find()) {
                      	   newphone = newmatcher.group(1);
                      	 
                      }else{
                    	  newnumPattern = Pattern.compile("TEL;type=WORK:+(.*?)\r");
                          newmatcher = newnumPattern.matcher(data);
                          if (newmatcher.find()) {
                          	   newphone = newmatcher.group(1);
                          	 
                          }else{
                        	  newnumPattern = Pattern.compile("TEL;TYPE=WORK:+(.*?)\r");
                              newmatcher = newnumPattern.matcher(data);
                              if (newmatcher.find()) {
                              	   newphone = newmatcher.group(1);
                              	 
                              }else{
            	  newnumPattern = Pattern.compile("TEL;type=WORK,VOICE:+(.*?)\n");
                  newmatcher = newnumPattern.matcher(data);
                  if (newmatcher.find()) {
                  	   newphone = newmatcher.group(1);
                  	 
              }else{
            	  newnumPattern = Pattern.compile("TEL;type=WORK,VOICE:+(.*?)\r");
                  newmatcher = newnumPattern.matcher(data);
                  if (newmatcher.find()) {
                  	   newphone = newmatcher.group(1);
                  	 
              }else{
        	  newnumPattern = Pattern.compile("TEL;TYPE=WORK,VOICE:+(.*?)\r");
              newmatcher = newnumPattern.matcher(data);
              if (newmatcher.find()) {
              	   newphone = newmatcher.group(1);
              	 
              }else{
            	  newnumPattern = Pattern.compile("TEL;TYPE=HOME,VOICE:+(.*?)\n");
            	  newmatcher = newnumPattern.matcher(data);
            	  if (newmatcher.find()) {
            		  newphone = newmatcher.group(1);
            		 
                 }else{
                	 newnumPattern = Pattern.compile("TEL;TYPE=HOME:+(.*?)\n");
               	  newmatcher = newnumPattern.matcher(data);
               	  if (newmatcher.find()) {
               		  newphone = newmatcher.group(1);
               		 
                    }else{
                	 newnumPattern = Pattern.compile("TEL;type=HOME,VOICE:+(.*?)\n");
               	  newmatcher = newnumPattern.matcher(data);
               	  if (newmatcher.find()) {
               		  newphone = newmatcher.group(1);
               		 
                    }else{
                    	newnumPattern = Pattern.compile("TEL;type=HOME:+(.*?)\n");
                     	  newmatcher = newnumPattern.matcher(data);
                     	  if (newmatcher.find()) {
                     		  newphone = newmatcher.group(1);
                     		 
                          }else{
                    	newnumPattern = Pattern.compile("TEL;type=HOME,VOICE:+(.*?)\r");
                     	  newmatcher = newnumPattern.matcher(data);
                     	  if (newmatcher.find()) {
                     		  newphone = newmatcher.group(1);
                     		 
                          }else{
                        	  newnumPattern = Pattern.compile("TEL;type=HOME:+(.*?)\r");
                         	  newmatcher = newnumPattern.matcher(data);
                         	  if (newmatcher.find()) {
                         		  newphone = newmatcher.group(1);
                         		 
                              }else{
                        	  newnumPattern = Pattern.compile("TEL;TYPE=HOME:+(.*?)\r");
                         	  newmatcher = newnumPattern.matcher(data);
                         	  if (newmatcher.find()) {
                         		  newphone = newmatcher.group(1);
                         		 
                              }else{
                	 newnumPattern = Pattern.compile("TEL;TYPE=HOME,VOICE:+(.*?)\r");
               	  newmatcher = newnumPattern.matcher(data);
               	  if (newmatcher.find()) {
               		  newphone = newmatcher.group(1);
               		 
                    }else{
                    	newnumPattern = Pattern.compile("TEL:+(.*?)\n");
                     	  newmatcher = newnumPattern.matcher(data);
                     	  if (newmatcher.find()) {
                     		  newphone = newmatcher.group(1);
                     		  
                          }else{
                        	  newnumPattern = Pattern.compile("TEL:+(.*?)\r");
                           	  newmatcher = newnumPattern.matcher(data);
                           	  if (newmatcher.find()) {
                           		  newphone = newmatcher.group(1);
                           		  
                                }else{
                                	newnumPattern = Pattern.compile("TEL;WORK:+(.*?)\n");
                               	  newmatcher = newnumPattern.matcher(data);
                               	  if (newmatcher.find()) {
                               		  newphone = newmatcher.group(1);
                               		 
                                    }else{
                                    	newnumPattern = Pattern.compile("TEL;WORK:+(.*?)\r");
                                   	  newmatcher = newnumPattern.matcher(data);
                                   	  if (newmatcher.find()) {
                                   		  newphone = newmatcher.group(1);
                                   		 
                                        }else{
                                        	newnumPattern = Pattern.compile("TEL;HOME:+(.*?)\n");
                                         	  newmatcher = newnumPattern.matcher(data);
                                         	  if (newmatcher.find()) {
                                         		  newphone = newmatcher.group(1);
                                         		  
                                              }else{
                                            	  newnumPattern = Pattern.compile("TEL;HOME:+(.*?)\r");
                                               	  newmatcher = newnumPattern.matcher(data);
                                               	  if (newmatcher.find()) {
                                               		  newphone = newmatcher.group(1);
                                               		  
                                                    }else{
                                                    	newnumPattern = Pattern.compile("TEL:+(.*?);");
                                                   	  newmatcher = newnumPattern.matcher(data);
                                                   	  if (newmatcher.find()) {
                                                   		  newphone = newmatcher.group(1);
                                                   		  
                                                        }else{
                                                        	newnumPattern = Pattern.compile("TEL;type=CELL:+(.*?)\n");
                                                         	  newmatcher = newnumPattern.matcher(data);
                                                         	  if (newmatcher.find()) {
                                                         		  newphone = newmatcher.group(1);
                                                         		  
                                                              }else{
                                                            	  newnumPattern = Pattern.compile("TEL;type=CELL:+(.*?)\r");
                                                               	  newmatcher = newnumPattern.matcher(data);
                                                               	  if (newmatcher.find()) {
                                                               		  newphone = newmatcher.group(1);
                                                               		  
                                                                    }else{
                                                                    	newnumPattern = Pattern.compile("TEL;TYPE=CELL:+(.*?)\n");
                                                                     	  newmatcher = newnumPattern.matcher(data);
                                                                     	  if (newmatcher.find()) {
                                                                     		  newphone = newmatcher.group(1);
                                                                     		  
                                                                          }else{
                                                                        	  newnumPattern = Pattern.compile("TEL;TYPE=CELL:+(.*?)\r");
                                                                           	  newmatcher = newnumPattern.matcher(data);
                                                                           	  if (newmatcher.find()) {
                                                                           		  newphone = newmatcher.group(1);
                                                                           		  
                                                                                }else{
                                                                                	newnumPattern = Pattern.compile("TEL;WORK;VOICE:+(.*?)\r");
                                                                                 	  newmatcher = newnumPattern.matcher(data);
                                                                                 	  if (newmatcher.find()) {
                                                                                 		  newphone = newmatcher.group(1);
                                                                                 		  
                                                                                      }else{
                                                                                    	  newnumPattern = Pattern.compile("TEL;HOME;VOICE:+(.*?)\r");
                                                                                       	  newmatcher = newnumPattern.matcher(data);
                                                                                       	  if (newmatcher.find()) {
                                                                                       		  newphone = newmatcher.group(1);
                                                                                       		  
                                                                                           }else{
                                                                                        	   newnumPattern = Pattern.compile("TEL;TYPE=cell:+(.*?)\r");
                                                                                            	  newmatcher = newnumPattern.matcher(data);
                                                                                            	  if (newmatcher.find()) {
                                                                                            		  newphone = newmatcher.group(1);
                                                                                            		  
                                                                                            	  }else{
                                                                                            		  newnumPattern = Pattern.compile("TEL;TYPE=cell:+(.*?)\n");
                                                                                                   newmatcher = newnumPattern.matcher(data);
                                                                                                   if (newmatcher.find()) {
                                                                                                   newphone = newmatcher.group(1);
                                                                                                   		  
                                                                                                   }else{
                                                                                                	   newnumPattern = Pattern.compile("TEL;+(.*?)\n");
                                                                                                       newmatcher = newnumPattern.matcher(data);
                                                                                                       if (newmatcher.find()) {
                                                                                                    	   String[] telParts =  newmatcher.group(1).split(":");
                                                                                                    	   if(telParts.length>1){
                                                                                                    		   newphone = telParts[telParts.length-1];
                                                                                                    	   }else{
                                                                                                    		   newphone = newmatcher.group(1);
                                                                                                    	   }
                                                                                                                                                                                                              		  
                                                                                                       }else{
                                                                                                    	   newnumPattern = Pattern.compile("TEL;+(.*?)\r");
                                                                                                           newmatcher = newnumPattern.matcher(data);
                                                                                                           if (newmatcher.find()) {
                                                                                                        	   String[] telParts =  newmatcher.group(1).split(":");
                                                                                                        	   if(telParts.length>1){
                                                                                                        		   newphone = telParts[telParts.length-1];
                                                                                                        	   }else{
                                                                                                        		   newphone = newmatcher.group(1);
                                                                                                        	   }
                                                                                                                                                                                                                  		  
                                                                                                           }
                                                                                                       }
                                                                                                   }
                                                                                            	  }
                                                                                           }
                                                                                      }
                                                                                }
                                                                          }
                                                                    }
                                                              }
                                                        }
                                                    }
                                              }
                                        }
                                    }
                                }  }
                    }}   }
                    } }  }
                    }       }         }    } }
                              }   }
           }
              } }
              
              Pattern neworgPattern = Pattern.compile("ORG:+(.*?)\n");
              newmatcher = neworgPattern.matcher(data);
              if (newmatcher.find()) {
              	   neworg = newmatcher.group(1);
              	 
              }else{
            	  neworgPattern = Pattern.compile("ORG:+(.*?)\r");
                  newmatcher = neworgPattern.matcher(data);
                  if (newmatcher.find()) {
                  	   neworg = newmatcher.group(1);
                  	 
                  }else{
                	  neworgPattern = Pattern.compile("ORG:+(.*?);");
                      newmatcher = neworgPattern.matcher(data);
                      if (newmatcher.find()) {
                      	   neworg = newmatcher.group(1);
                      	 
                      }else{
                    	  neworgPattern = Pattern.compile("ORG;+(.*?)\n");
                          newmatcher = neworgPattern.matcher(data);
                          if (newmatcher.find()) {
                        	  String[] orgParts = newmatcher.group(1).split(":");
                        	  if(orgParts.length>1){
                        		  neworg = orgParts[orgParts.length-1];
                        	  }else{
                        		  neworg = newmatcher.group(1); 
                        	  }
                          	  
                          	 
                          }else{
                        	  neworgPattern = Pattern.compile("ORG;+(.*?)\r");
                              newmatcher = neworgPattern.matcher(data);
                              if (newmatcher.find()) {
                            	  String[] orgParts = newmatcher.group(1).split(":");
                            	  if(orgParts.length>1){
                            		  neworg = orgParts[orgParts.length-1];
                            	  }else{
                            		  neworg = newmatcher.group(1); 
                            	  }
                              	  
                              	 
                              }
                          }
                      }
                  } 
              }
              
              Pattern newtitlePattern = Pattern.compile("TITLE:+(.*?)\n");
              newmatcher = newtitlePattern.matcher(data);
              if (newmatcher.find()) {
              	   newtitle = newmatcher.group(1);
              	 
              }else{
            	  newtitlePattern = Pattern.compile("TITLE:+(.*?)\r");
                  newmatcher = newtitlePattern.matcher(data);
                  if (newmatcher.find()) {
                  	   newtitle = newmatcher.group(1);
                  	 
                  }else{
	            	  newtitlePattern = Pattern.compile("TITLE:+(.*?);");
	                  newmatcher = newtitlePattern.matcher(data);
	                  if (newmatcher.find()) {
	                  	   newtitle = newmatcher.group(1);
	                  	 
	                  } 
                  }
              }
              
              if(newcurrentapiVersion>=11){
              
            	//Email
            	  ContentValues row1 = new ContentValues();
            	  row1.put(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE);
            	  row1.put(Email.ADDRESS, newemail);
            	  newtext.add(row1);
            	  
            	 
            	  
            	//Website
            	  ContentValues row2 = new ContentValues();
            	  row2.put(Data.MIMETYPE, Website.CONTENT_ITEM_TYPE);
            	  row2.put(Website.URL, newwebsit);
            	  newtext.add(row2);
            	  
            	  
            	  //name
            	  ContentValues row3 = new ContentValues();
            	  row3.put(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE);
            	  row3.put(StructuredName.DISPLAY_NAME, newname);
            	  newtext.add(row3);
            	  
            	  
            	  
            	  //Address
            	  String [] newlist = newaddress.split(" ");
            	  
            	  ContentValues row4 = new ContentValues();
            	  row4.put(Data.MIMETYPE, StructuredPostal.CONTENT_ITEM_TYPE);
            	  row4.put(StructuredPostal.CITY, newlist[0]);
            	  row4.put(StructuredPostal.COUNTRY, newlist[3]);
            	  row4.put(StructuredPostal.STREET, newlist[1]);
            	  row4.put(StructuredPostal.POSTCODE, newlist[2]);
            	  newtext.add(row4);
            	  
            	  
            	  
            	  //Phone 
            	  
            	  ContentValues row5 = new ContentValues();
            	  row5.put(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
            	  row5.put(Phone.NUMBER, newphone);
            	  newtext.add(row5);
            	  
            	  //company
            	  
            	  ContentValues row6 = new ContentValues();
            	  row6.put(Data.MIMETYPE, Organization.CONTENT_ITEM_TYPE);
            	  row6.put(Organization.COMPANY, neworg);
            	  newtext.add(row6);
            	  
            	  //title
            	  
            	  ContentValues row7 = new ContentValues();
            	  row7.put(Data.MIMETYPE, Note.CONTENT_ITEM_TYPE);
            	  row7.put(Note.NOTE, newtitle);
            	  newtext.add(row7);
            	  
            		  Intent i = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
                	  i.putParcelableArrayListExtra(Insert.DATA, newtext);
                	  startActivity(i);
        	  }else{
        		  Intent i = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
        		  i.setType(ContactsContract.Contacts.CONTENT_TYPE);
        		  i.putExtra(ContactsContract.Intents.Insert.NAME, newname);
        		  i.putExtra(ContactsContract.Intents.Insert.PHONE, newphone);
        		  i.putExtra(ContactsContract.Intents.Insert.POSTAL, newaddress);
        		  i.putExtra(ContactsContract.Intents.Insert.EMAIL, newemail);
        		  i.putExtra(ContactsContract.Intents.Insert.NOTES, newwebsit);
        		  i.putExtra(ContactsContract.Intents.Insert.COMPANY, neworg);
        		  i.putExtra(ContactsContract.Intents.Insert.JOB_TITLE, newtitle);
        		  startActivity(i);
        	  }
			
			break;
			
		case 8:
			
			ArrayList<ContentValues> nexttext = new ArrayList<ContentValues>();
        	String nextname="",nextemail="",nextaddress="",nextwebsit="",nextphone="", nextorg="", nexttitle="";
        	
        	int nextcurrentapiVersion = android.os.Build.VERSION.SDK_INT;
             Pattern nextemailPattern = Pattern.compile("E:+(.*?);");
             Matcher nextmatcher = nextemailPattern.matcher(data);
             if (nextmatcher.find()) {
            	 nextemail = nextmatcher.group(1);
             }
        	
             Pattern nexturlPattern = Pattern.compile("URL:+(.*?);");
             nextmatcher = nexturlPattern.matcher(data);
              if (nextmatcher.find()) {
              	   nextwebsit = nextmatcher.group(1);
              }
             
              
              Pattern nextnamePattern = Pattern.compile("N:+(.*?);");
              nextmatcher = nextnamePattern.matcher(data);
              if (nextmatcher.find()) {
              	   nextname = nextmatcher.group(1);
              }
              
              Pattern nextaddPattern = Pattern.compile("A:+(.*?);");
              nextmatcher = nextaddPattern.matcher(data);
              if (nextmatcher.find()) {
              	   nextaddress = nextmatcher.group(1);
              }
              
              Pattern nextnumPattern = Pattern.compile("B:+(.*?);");
              nextmatcher = nextnumPattern.matcher(data);
              if (nextmatcher.find()) {
              	   nextphone = nextmatcher.group(1);
              }
              Pattern nextorgPattern = Pattern.compile("C:+(.*?);");
              nextmatcher = nextorgPattern.matcher(data);
              if (nextmatcher.find()) {
              	   nextorg = nextmatcher.group(1);
              }
              
              Pattern nexttitlePattern = Pattern.compile("T:+(.*?);");
              nextmatcher = nexttitlePattern.matcher(data);
              if (nextmatcher.find()) {
              	   nexttitle = nextmatcher.group(1);
              	 
              }
              
              if(nextcurrentapiVersion>=11){
              
            	//Email
            	  ContentValues row1 = new ContentValues();
            	  row1.put(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE);
            	  row1.put(Email.ADDRESS, nextemail);
            	  nexttext.add(row1);
            	  
            	 
            	  
            	//Website
            	  ContentValues row2 = new ContentValues();
            	  row2.put(Data.MIMETYPE, Website.CONTENT_ITEM_TYPE);
            	  row2.put(Website.URL, nextwebsit);
            	  nexttext.add(row2);
            	  
            	  
            	  //name
            	  ContentValues row3 = new ContentValues();
            	  row3.put(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE);
            	  row3.put(StructuredName.DISPLAY_NAME, nextname);
            	  nexttext.add(row3);
            	  
            	  
            	  
            	  //Address
            	  String [] list = nextaddress.split(" ");
            	  
            	  ContentValues row4 = new ContentValues();
            	  row4.put(Data.MIMETYPE, StructuredPostal.CONTENT_ITEM_TYPE);
            	  row4.put(StructuredPostal.CITY, list[0]);
            	  row4.put(StructuredPostal.COUNTRY, list[3]);
            	  row4.put(StructuredPostal.STREET, list[1]);
            	  row4.put(StructuredPostal.POSTCODE, list[2]);
            	  nexttext.add(row4);
            	  
            	  
            	  
            	  //Phone 
            	  
            	  ContentValues row5 = new ContentValues();
            	  row5.put(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
            	  row5.put(Phone.NUMBER, nextphone);
            	  nexttext.add(row5);
            	  
            	  //company
            	  
            	  ContentValues row6 = new ContentValues();
            	  row6.put(Data.MIMETYPE, Organization.CONTENT_ITEM_TYPE);
            	  row6.put(Organization.COMPANY, nextorg);
            	  nexttext.add(row6);
            	  
            	  //title
            	  
            	  ContentValues row7 = new ContentValues();
            	  row7.put(Data.MIMETYPE, Note.CONTENT_ITEM_TYPE);
            	  row7.put(Note.NOTE, nexttitle);
            	  nexttext.add(row7);
            	  
            	  
            		  Intent i = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
                	  i.putParcelableArrayListExtra(Insert.DATA, nexttext);
                	  startActivity(i);
        	  }else{
        		  Intent i = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
        		  i.setType(ContactsContract.Contacts.CONTENT_TYPE);
        		  i.putExtra(ContactsContract.Intents.Insert.NAME, nextname);
        		  i.putExtra(ContactsContract.Intents.Insert.PHONE, nextphone);
        		  i.putExtra(ContactsContract.Intents.Insert.POSTAL, nextaddress);
        		  i.putExtra(ContactsContract.Intents.Insert.EMAIL, nextemail);
        		  i.putExtra(ContactsContract.Intents.Insert.NOTES, nextwebsit);
        		  i.putExtra(ContactsContract.Intents.Insert.COMPANY, nextorg);
        		  i.putExtra(ContactsContract.Intents.Insert.JOB_TITLE, nexttitle);
        		  startActivity(i);
        	  }
			
			break;
			
		case 9:
			
			String subject="",body="",recipent="";
			
			Pattern recipientPattern = Pattern.compile("TO:+(.*?);");
            Matcher emailMatcher = recipientPattern.matcher(data);
            if (emailMatcher.find()) {
            	recipent = emailMatcher.group(1);
            }
            
            Pattern subjectPattern = Pattern.compile("SUB:+(.*?);");
            Matcher subjectMatcher = subjectPattern.matcher(data);
            if (subjectMatcher.find()) {
            	subject = subjectMatcher.group(1);
            }
            
            Pattern bodyPattern = Pattern.compile("SUB:+(.*?);");
            Matcher bodyMatcher = bodyPattern.matcher(data);
            if (bodyMatcher.find()) {
            	body = bodyMatcher.group(1);
            }
			
			Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("message/rfc822");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{recipent});
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);

            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
			
			break;
			
		
		}
	}
}
